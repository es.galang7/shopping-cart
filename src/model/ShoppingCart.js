class ShoppingCart{
    // Default value firstMonthStatus (If its the subscribers first month of subscription) is true
    constructor(pricingRules, firstMonthStatus = true) {
        this.pricingRules = pricingRules;
        this.itemList = [];
        this.promoCode = null;
        this.firstMonthStatus = firstMonthStatus;
    }

    // Add items and promo code to the cart
    add(item, promoCode = null){
        this.itemList.push(item);
        if(promoCode){
            this.promoCode = promoCode;
        }
    }

    // Calculate total
    total(){
        const itemCounts = this.countItems();
        let total = 0;

        for ( const item in itemCounts) {
            if(itemCounts.hasOwnProperty(item) && this.pricingRules.rules.hasOwnProperty(item)){
                total += this.pricingRules.calculatePrice(item, itemCounts[item], this.firstMonthStatus);
            }
        }

        if (this.pricingRules.promoCodes.hasOwnProperty(this.promoCode)) {
            const discount = this.pricingRules.promoCodes[this.promoCode];
            total *= (1 - (discount/100));
        }

        return total.toFixed(2);
    }

    // Count the types of items in the cart
    countItems() {
        const itemCounts = {};

        for( const item of this.itemList) {
            itemCounts[item] = itemCounts[item] ? itemCounts[item] + 1 : 1;
        }

        return itemCounts;
    }

    // List down items in the cart with handling for the bundles
    items(){
        const itemCounts = this.countItems();
        const distinctItemList = [...new Set(this.itemList)];
        for (const key of  distinctItemList) {
                if (this.pricingRules.bundles.hasOwnProperty(key)) {
                    const bundledItem = this.pricingRules.bundles[key];
                    itemCounts[bundledItem] = itemCounts[bundledItem] ? itemCounts[bundledItem] + itemCounts[key]
                        : itemCounts[key];
                }
        }

        return itemCounts;
    }

}

export default ShoppingCart;
