class PricingRules{

    // constructor parameters are bundles and promo code
    constructor(bundleConfig, promoCodeConfig) {
        this.rules = {
            'ult_small': (count, firstMonthStatus) =>{
                if(firstMonthStatus && count >= 3){
                    const discountedCount = Math.floor(count / 3) * 2 + (count % 3);
                    return discountedCount * 24.90;
                }else{
                    return count * 24.90;
                }

            },

            'ult_medium': (count) =>{
                return count * 29.90;
            },

            'ult_large': (count, firstMonthStatus) =>{
                if (firstMonthStatus && count > 3) {
                    return count * 39.90;
                }
                return count * 44.90;
            },

            '1gb': (count) =>{
                return count * 9.90;
            },
        };

        this.bundles = bundleConfig || {};
        this.promoCodes = promoCodeConfig || {};
    }

    // Add price rules, parameters are item: unique key for the item, ruleFunction
    // For the rule function parameter, parameters like firstMonthStatus can
    // be used to customize the handling of prize rules

    addRule(item, ruleFunction) {
        this.rules[item] = ruleFunction;
    }

    // Set bundles dynamically, overwrite initial bundle
    setBundles(bundleConfig) {
        this.bundles = bundleConfig || {};
    }

    // Set promo codes dynamically, overwrite initial promo code
    setPromoCodes(promoCodeConfig) {
        this.promoCodes = promoCodeConfig || {};
    }

    // Calculate price based on rules
    calculatePrice(item, count, firstMonthStatus) {
        if (this.rules.hasOwnProperty(item)) {
            return this.rules[item](count, firstMonthStatus);
        }
        return 0; // Default to 0 if the item is not found in rules
    }

}

export default PricingRules;