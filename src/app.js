import ShoppingCart from './model/ShoppingCart.js';
import PricingRules from './model/PricingRules.js';


const pricingRules = new PricingRules();
pricingRules.addRule('ult_extra_large', (count) => {
        return count * 50.00;
});
pricingRules.setBundles({'ult_medium':'1gb'})
pricingRules.setPromoCodes({'I<3AMAYSIM':'10'});
const cart = new ShoppingCart(pricingRules);
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_medium');
cart.add('ult_extra_large', 'I<3AMAYSIM');
const total = cart.total();
const items = cart.items();
console.log("Items: " + JSON.stringify(items, null, 2));
console.log("Total Price: $" + total);