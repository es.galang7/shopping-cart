# Amaysim Shopping Cart Exercise

## Requirements:

- Node must be installed
- Source code accessible in https://gitlab.com/es.galang7/shopping-cart

## Test and Deploy:

- Run npm install
- To run source code; run command in terminal 'npm run start'
- To run scenario test; run command in terminal 'npm run test'

## Description
Shopping Cart Exercise.

