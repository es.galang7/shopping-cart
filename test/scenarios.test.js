import { expect } from 'chai';
import PricingRules from '../src/model/PricingRules.js'
import ShoppingCart from '../src/model/ShoppingCart.js';

describe('ShoppingCart', () => {
    // Define pricing rules and promo codes
    let expectedItems;
    let total;
    let items;
    const pricingRules = new PricingRules(
        {'ult_medium':'1gb' },
        {'I<3AMAYSIM':'10'}
    );

    it('Scenario 1: Should calculate the cart total correctly', () => {
        // 3 x Unlimited 1 GB
        // 1 x Unlimited 5 GB
        const cart = new ShoppingCart(pricingRules, true);

        cart.add('ult_small');
        cart.add('ult_small');
        cart.add('ult_small');
        cart.add('ult_large');

        total = cart.total();
        items = cart.items();

        expectedItems = {
            ult_small: 3,
            ult_large: 1
        }
        // Expected total: (49.80 + 44.90) = 94.70
        expect(total).to.equal('94.70');
        expect(items).deep.equal(expectedItems);
    });

    it('Scenario 2: Should calculate the cart total with discount deal correctly', () => {
        //2 x Unlimited 1 GB
        //4 x Unlimited 5 GB
        const cart = new ShoppingCart(pricingRules, true);

        cart.add('ult_small');
        cart.add('ult_small');
        cart.add('ult_large');
        cart.add('ult_large');
        cart.add('ult_large');
        cart.add('ult_large');

        total = cart.total();
        items = cart.items();

        expectedItems = {
            ult_small: 2,
            ult_large: 4
        }
        // Expected total: (49.80 + 159.60) = 209.40
        expect(total).to.equal('209.40');
        expect(items).deep.equal(expectedItems);
    });

    it('Scenario 3: Should calculate the cart total with bundle correctly', () => {
        const cart = new ShoppingCart(pricingRules, true);

        // 1 x Unlimited 1 GB
        // 2 X Unlimited 2 GB

        cart.add('ult_small');
        cart.add('ult_medium');
        cart.add('ult_medium');

        total = cart.total();
        items = cart.items();

        expectedItems = {
            ult_small: 1,
            ult_medium: 2,
            '1gb': 2,
        }
        // Expected total: (24.90 + 59.80) = 84.70
        expect(total).to.equal('84.70');
        expect(items).deep.equal(expectedItems);
    });

    it('Scenario 4: Should calculate the cart total with promo correctly', () => {
        const cart = new ShoppingCart(pricingRules, true);

        cart.add('ult_small');
        cart.add('1gb', 'I<3AMAYSIM'); // promo code

        const total = cart.total();

        // Expected total: (24.90 + 9.90) * 0.9 (promo code discount) = 31.32
        expect(total).to.equal('31.32');
    });
});